import psycopg2
import os
from tabulate import tabulate

conn_url = os.environ.get("CONN_URL")

conn = psycopg2.connect(conn_url)

cur = conn.cursor()

cur.execute("""
    SELECT sub_name, COUNT(sub_mark) FROM student_records
    JOIN subject ON student_records.sub_id = subject.sub_id
    GROUP BY sub_name
    ORDER BY sub_name ASC;
""")

results = cur.fetchall()

table_data = []
for row in results:
    table_data.append([row[0], row[1]])

table_headers = ["Subject", "Count"]
table = tabulate(table_data, headers=table_headers, tablefmt="grid")
print(table)

cur.close()
conn.close()
