CREATE TABLE IF NOT EXISTS student(
  st_id SERIAl PRIMARY KEY,
  st_name VARCHAR(128) NOT NULL,
  st_surname VARCHAR(128) NOT NULL,
  st_bdate DATE NOT NULL
);

CREATE TABLE IF NOT EXISTS subject(
  sub_id SERIAL PRIMARY KEY,
  sub_name VARCHAR(64) NOT NULL
);

CREATE TABLE IF NOT EXISTS student_records(
  rec_id SERIAl PRIMARY KEY,
  st_id INTEGER NOT NULL REFERENCES student(st_id),
  sub_id INTEGER NOT NULL REFERENCES subject(sub_id),
  sub_mark INTEGER DEFAULT NULL
);

INSERT INTO subject(sub_name)
VALUES('subject0'), ('subject1'), ('subject2'), ('subject3'), ('subject4');

INSERT INTO student(st_name, st_surname, st_bdate)
VALUES ('name0', 'surname0', '1992.12.04'),
('name1', 'surname1', '1993.03.09'),
('name2', 'surname2', '1994.09.12'),
('name3', 'surname3', '1994.02.18'),
('name4', 'surname4', '1995.12.30'),
('name5', 'surname5', '1995.10.13'),
('name6', 'surname6', '1997.09.01');

INSERT INTO student_records(st_id, sub_id)
VALUES(1, 2),
(4, 1),
(7, 2),
(7, 4);

INSERT INTO student_records(st_id, sub_id, sub_mark)
VALUES(1, 5, 3),
(2, 4, 4),
(3, 1, 4),
(3, 2, 5),
(3, 4, 5),
(4, 3, 2),
(5, 2, 5),
(6, 4, 4),
(6, 5, 3),
(7, 3, 5);
