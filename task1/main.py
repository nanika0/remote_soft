import os


def is_char(variable):
    return isinstance(variable, str) and len(variable) == 1


def is_digit(variable):
    return variable.isdigit()


def int_conv(variable):
    return int(variable)


def var_cond(variable):
    if is_char(variable) and is_digit(variable):
        return int_conv(variable)
    return 'Not a digit'


if __name__ == '__main__':
    input_var = os.environ.get('input_var')
    print(var_cond(input_var))
